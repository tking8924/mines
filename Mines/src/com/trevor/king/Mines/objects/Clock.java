package com.trevor.king.Mines.objects;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class Clock implements WindowListener {

    private boolean running = false;
    private boolean visible = false;
    private long startTime;
    private long currentTime;
    private long storedTime = 0;

    public void start() {
        running = true;
        startTime = System.nanoTime();
    }

    public void update() {
        if (running && visible) {
            currentTime = System.nanoTime();
            storedTime += currentTime - startTime;
            startTime = currentTime;
        }
    }

    public void stop() {
        running = false;
        storedTime += currentTime - startTime;
    }

    public int getElapsedSeconds() {
        return (int) (storedTime / 1000000000);
    }

    public void reset() {
        storedTime = 0;
    }

    public void windowActivated(WindowEvent e) {
        visible = true;
        startTime = System.nanoTime();
    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {
        visible = false;
        update();
    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowOpened(WindowEvent e) {

    }

}
