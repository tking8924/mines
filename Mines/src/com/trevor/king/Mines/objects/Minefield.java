package com.trevor.king.Mines.objects;

import java.util.Random;

import com.trevor.king.Mines.graphics.Screen;

public class Minefield {

    private Mine[] mines;
    public int numMines;
    public int fieldX;
    public int fieldY;
    public int mineSizeX;
    public int mineSizeY;
    private Screen screen;
    private int originX;
    private int originY;
    private int maxX;
    private int maxY;
    private int buffer;

    public Minefield(int x, int y, int nmines, int mineSizeX, int mineSizeY, int buffer, int edge, Screen screen) {
        fieldX = x;
        fieldY = y;
        numMines = nmines;
        this.screen = screen;
        this.mineSizeX = mineSizeX;
        this.mineSizeY = mineSizeY;
        this.buffer = buffer;
        mines = new Mine[fieldX * fieldY];

        originX = edge;
        originY = edge;
        maxX = fieldX * (mineSizeX + buffer) + originX;
        maxY = fieldY * (mineSizeY + buffer) + originY;

        generateField();
    }

    public void draw() {
        int posX;
        int posY;

        for (int x = 0; x < fieldX; x++) {
            for (int y = 0; y < fieldY; y++) {
                if (x == 0) posX = (x * mineSizeX) + originX;
                else
                    posX = x * (mineSizeX + buffer) + originX;
                if (y == 0) posY = (y * mineSizeY) + originY;
                else
                    posY = y * (mineSizeY + buffer) + originY;
                screen.render(posX, posY, mineSizeX, mineSizeY, mines[x + y * fieldX].draw());
            }
        }
    }

    private void generateField() {
        int[][] BOARD = new int[fieldX][fieldY];
        Random r = new Random();
        for (int i = 0; i < numMines; i++) {
            int mine_x = r.nextInt(fieldX);
            int mine_y = r.nextInt(fieldY);
            if (BOARD[mine_x][mine_y] == 9) {
                i -= 1;
            } else {
                BOARD[mine_x][mine_y] = 9;
            }
        }

        for (int i = 0; i < fieldX; i++) {
            for (int j = 0; j < fieldY; j++) {
                if (BOARD[i][j] != 9) {
                    int check_x = i - 1;
                    int check_y = j - 1;
                    int count = 0;

                    if ((check_x >= 0) && (check_y >= 0) && (check_x < fieldX) && (check_y < fieldY)) {
                        if (BOARD[check_x][check_y] == 9) count++;
                    }
                    check_y += 1;
                    if ((check_x >= 0) && (check_y >= 0) && (check_x < fieldX) && (check_y < fieldY)) {
                        if (BOARD[check_x][check_y] == 9) count++;
                    }
                    check_y += 1;
                    if ((check_x >= 0) && (check_y >= 0) && (check_x < fieldX) && (check_y < fieldY)) {
                        if (BOARD[check_x][check_y] == 9) count++;
                    }
                    check_x += 1;
                    if ((check_x >= 0) && (check_y >= 0) && (check_x < fieldX) && (check_y < fieldY)) {
                        if (BOARD[check_x][check_y] == 9) count++;
                    }
                    check_x += 1;
                    if ((check_x >= 0) && (check_y >= 0) && (check_x < fieldX) && (check_y < fieldY)) {
                        if (BOARD[check_x][check_y] == 9) count++;
                    }
                    check_y -= 1;
                    if ((check_x >= 0) && (check_y >= 0) && (check_x < fieldX) && (check_y < fieldY)) {
                        if (BOARD[check_x][check_y] == 9) count++;
                    }
                    check_y -= 1;
                    if ((check_x >= 0) && (check_y >= 0) && (check_x < fieldX) && (check_y < fieldY)) {
                        if (BOARD[check_x][check_y] == 9) count++;
                    }
                    check_x -= 1;
                    if ((check_x >= 0) && (check_y >= 0) && (check_x < fieldX) && (check_y < fieldY)) {
                        if (BOARD[check_x][check_y] == 9) count++;
                    }
                    BOARD[i][j] = count;
                }
            }
        }

        for (int x = 0; x < fieldX; x++) {
            for (int y = 0; y < fieldY; y++) {
                mines[x + y * fieldX] = new Mine(BOARD[x][y]);
            }
        }
    }

    public boolean checkMine(int x, int y) {
        if (x <= maxX && x >= originX && y <= maxY && y >= originY) {
            int mineX = (x - originX) / (mineSizeX + buffer);
            int mineY = (y - originY) / (mineSizeY + buffer);
            int prox = mines[mineX + mineY * fieldX].select();
            if (prox == 0) return checkCloseBy(mineX, mineY);
            if (prox == 9) return true;
            else
                return false;
        }
        return false;
    }

    public int markMine(int x, int y) {
        if (x <= maxX && x >= originX && y <= maxY && y >= originY) {
            int mineX = (x - originX) / (mineSizeX + buffer);
            int mineY = (y - originY) / (mineSizeY + buffer);
            return mines[mineX + mineY * fieldX].mark();
        }
        return 0;
    }

    public boolean checkRelativeMine(int x, int y) {
        if (x < fieldX && x >= 0 && y <= fieldY && y >= 0) {
            int mineX = x;
            int mineY = y;
            int prox = mines[mineX + mineY * fieldX].select();
            if (prox == 0) checkCloseBy(mineX, mineY);
            if (prox == 9) return true;
            else
                return false;
        }
        return false;
    }

    public void showAll() {
        for (int i = 0; i < mines.length; i++) {
            mines[i].select();
        }
    }

    public boolean checkWin() {
        for (int i = 0; i < mines.length; i++) {
            if (mines[i].proximity != 9 && !mines[i].shown) return false;
        }

        return true;
    }

    public boolean checkCloseBy(int x, int y) {
        boolean mine = false;
        x -= 1;
        y -= 1;
        if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && !mines[x + y * fieldX].shown && !mines[x + y * fieldX].marked) {
            if (checkRelativeMine(x, y)) mine = true;
        }
        y += 1;
        if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && !mines[x + y * fieldX].shown && !mines[x + y * fieldX].marked) {
            if (checkRelativeMine(x, y)) mine = true;
        }
        y += 1;
        if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && !mines[x + y * fieldX].shown && !mines[x + y * fieldX].marked) {
            if (checkRelativeMine(x, y)) mine = true;
        }
        x += 1;
        if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && !mines[x + y * fieldX].shown && !mines[x + y * fieldX].marked) {
            if (checkRelativeMine(x, y)) mine = true;
        }
        x += 1;
        if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && !mines[x + y * fieldX].shown && !mines[x + y * fieldX].marked) {
            if (checkRelativeMine(x, y)) mine = true;
        }
        y -= 1;
        if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && !mines[x + y * fieldX].shown && !mines[x + y * fieldX].marked) {
            if (checkRelativeMine(x, y)) mine = true;
        }
        y -= 1;
        if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && !mines[x + y * fieldX].shown && !mines[x + y * fieldX].marked) {
            if (checkRelativeMine(x, y)) mine = true;
        }
        x -= 1;
        if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && !mines[x + y * fieldX].shown && !mines[x + y * fieldX].marked) {
            if (checkRelativeMine(x, y)) mine = true;
        }
        return mine;
    }

    public boolean clearSurround(int xa, int ya) {
        if (xa <= maxX && xa >= originX && ya <= maxY && ya >= originY) {
            int x = (xa - originX) / (mineSizeX + buffer);
            int y = (ya - originY) / (mineSizeY + buffer);
            int xb = x;
            int yb = y;

            if (mines[x + y * fieldX].shown) {
                int num = mines[x + y * fieldX].proximity;
                int marked = 0;
                x -= 1;
                y -= 1;
                if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && mines[x + y * fieldX].marked) {
                    marked++;
                }
                y += 1;
                if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && mines[x + y * fieldX].marked) {
                    marked++;
                }
                y += 1;
                if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && mines[x + y * fieldX].marked) {
                    marked++;
                }
                x += 1;
                if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && mines[x + y * fieldX].marked) {
                    marked++;
                }
                x += 1;
                if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && mines[x + y * fieldX].marked) {
                    marked++;
                }
                y -= 1;
                if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && mines[x + y * fieldX].marked) {
                    marked++;
                }
                y -= 1;
                if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && mines[x + y * fieldX].marked) {
                    marked++;
                }
                x -= 1;
                if ((x >= 0) && (y >= 0) && (x < fieldX) && (y < fieldY) && mines[x + y * fieldX].marked) {
                    marked++;
                }
                if (marked == num) { return checkCloseBy(xb, yb); }
            }
        }
        return false;
    }

}
