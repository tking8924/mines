package com.trevor.king.Mines.objects;

import com.trevor.king.Mines.graphics.Sprite;

public class Mine {
    public int proximity;
    public boolean shown = false;
    public boolean marked = false;
    private Sprite sprite = Sprite.normal;

    public Mine(int proximity) {
        this.proximity = proximity;
    }

    public int[] draw() {
        return sprite.pixels;
    }

    public int select() {
        if (!marked) {
            shown = true;
            if (proximity == 9) sprite = Sprite.mine;
            else if (proximity == 1) sprite = Sprite.one;
            else if (proximity == 2) sprite = Sprite.two;
            else if (proximity == 3) sprite = Sprite.three;
            else if (proximity == 4) sprite = Sprite.four;
            else if (proximity == 5) sprite = Sprite.five;
            else if (proximity == 6) sprite = Sprite.six;
            else if (proximity == 7) sprite = Sprite.seven;
            else if (proximity == 8) sprite = Sprite.eight;
            else sprite = Sprite.zero;
            return proximity;
        }

        return -1;
    }

    public int mark() {
        if (!shown) {
            if (marked) {
                marked = false;
                sprite = Sprite.normal;
                return 1;
            } else {
                marked = true;
                sprite = Sprite.flag;
                return 2;
            }
        }
        return 0;
    }
}
