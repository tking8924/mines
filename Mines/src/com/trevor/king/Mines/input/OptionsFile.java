package com.trevor.king.Mines.input;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class OptionsFile {

    // Number of Options
    private static final int numOptions = 6;
    private String[] options = new String[numOptions];

    // Options
    private int numMines;
    private int sizeX;
    private int sizeY;
    private int played;
    private int won;
    private String bestTimeString;
    private Map<String, Integer> bestTimes = new HashMap<String, Integer>();

    // Path
    private String path;

    public OptionsFile(String path) {
        this.path = path;
    }

    public boolean read() {
        try {
            File opts = new File(path);
            if (!(opts.exists())) return false;
            BufferedReader opFile = new BufferedReader(new FileReader(opts));
            String line;
            int index = 0;
            while ((line = opFile.readLine()) != null) {
                options[index] = line;
                index++;
            }
            opFile.close();
        } catch (IOException e) {
            System.err.println(e);
        }

        try {
            numMines = Integer.parseInt(options[0]);
            sizeX = Integer.parseInt(options[1]);
            sizeY = Integer.parseInt(options[2]);
            played = Integer.parseInt(options[3]);
            won = Integer.parseInt(options[4]);
            bestTimeString = options[5];
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return processBestTimeString();
    }

    private boolean processBestTimeString() {
        try {
            String[] temp = bestTimeString.split(":");
            if ((temp.length % 2) != 0) return false;
            for (int i = 0; i < temp.length; i++) {
                bestTimes.put(temp[i], Integer.parseInt(temp[i + 1]));
                i += 1;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String createBestTimeString() {
        Iterator<String> it = bestTimes.keySet().iterator();
        String s = "";
        boolean first = true;
        while (it.hasNext()) {
            String key = it.next();
            String value = String.valueOf(bestTimes.get(key));
            if (!first) s += ":";
            s += key + ":" + value;
            first = false;
        }

        return s;
    }

    public void write(int numMines, int sizeX, int sizeY, int played, int won, int time) {
        this.numMines = numMines;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.played = played;
        this.won = won;

        String key = numMines + "," + sizeX + "," + sizeY;
        bestTimes.put(key, time);

        options[0] = String.valueOf(numMines);
        options[1] = String.valueOf(sizeX);
        options[2] = String.valueOf(sizeY);
        options[3] = String.valueOf(played);
        options[4] = String.valueOf(won);
        options[5] = createBestTimeString();

        try {
            BufferedWriter opFile = new BufferedWriter(new FileWriter(path));
            for (String option : options) {
                opFile.write(option);
                opFile.newLine();
            }
            opFile.close();
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public int getMines() {
        return numMines;
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public int getPlayed() {
        return played;
    }

    public int getWon() {
        return won;
    }

    public int getBestTime(int numMines, int sizeX, int sizeY) {
        String key = numMines + "," + sizeX + "," + sizeY;
        int bt;
        if (bestTimes.containsKey(key)) bt = bestTimes.get(key);
        else
            bt = 9999;
        return bt;
    }
}
