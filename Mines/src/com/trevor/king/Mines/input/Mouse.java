package com.trevor.king.Mines.input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Mouse implements MouseListener {

    public int x = 0;
    public int y = 0;
    public boolean leftClick = false;
    public boolean middleClick = false;
    public boolean rightClick = false;
    private boolean leftHold = false;
    private boolean rightHold = false;
    private boolean middleHold = false;

    public void mouseClicked(MouseEvent arg0) {

    }

    public void mouseEntered(MouseEvent arg0) {

    }

    public void mouseExited(MouseEvent arg0) {

    }

    public void mousePressed(MouseEvent arg0) {
        if (arg0.getButton() == MouseEvent.BUTTON1) leftHold = true;
        if (arg0.getButton() == MouseEvent.BUTTON2) middleHold = true;
        if (arg0.getButton() == MouseEvent.BUTTON3) rightHold = true;
    }

    public void mouseReleased(MouseEvent arg0) {
        if (leftHold && rightHold) {
            middleHold = true;
            leftHold = false;
            rightHold = false;
        }
        leftClick = leftHold;
        rightClick = rightHold;
        middleClick = middleHold;
        leftHold = false;
        rightHold = false;
        middleHold = false;
        x = arg0.getX();
        y = arg0.getY();
    }

    public void reset() {
        leftClick = false;
        middleClick = false;
        rightClick = false;
    }

}
