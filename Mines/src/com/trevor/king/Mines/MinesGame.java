package com.trevor.king.Mines;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.trevor.king.Mines.graphics.Screen;
import com.trevor.king.Mines.input.Mouse;
import com.trevor.king.Mines.input.OptionsFile;
import com.trevor.king.Mines.objects.Clock;
import com.trevor.king.Mines.objects.Minefield;

public class MinesGame extends Canvas implements Runnable {

    private static final long serialVersionUID = 3593448300774284495L;
    public static final String GAME_TITLE = "Mines";
    public static final String GAME_STAGE = "Release";
    public static final int GAME_MAJOR_VERSION = 1;
    public static final int GAME_MINOR_VERSION = 0;
    public static final int GAME_REVISION = 0;

    private static MinesGame t;

    private boolean running = false;
    private static boolean debug = false;
    private boolean gameOver = false;
    private boolean loss = false;
    private boolean firstClick = false;
    private boolean clockStarted = false;

    public static int WIDTH;
    public static int HEIGHT;
    public static final int MIN_WIDTH = 300;
    public static final int MIN_HEIGHT = 200;
    public static final int MINE_SIZE = 32;
    public static final int BUFFER = 2;
    public static final int EDGE = 40;
    public static int FIELD_X;
    public static int FIELD_Y;
    public static int NUM_MINES;

    private BufferedImage image;
    private int[] pixels;
    private BufferStrategy bs;
    private static JFrame frame;

    private Thread thread;

    private Screen screen;
    private Mouse mouse;
    private Minefield mines;
    private static OptionsFile options;
    public Clock clock;

    private int fps;
    private int tps;

    private static int played;
    private static int won;
    private int score;
    private int elapsedTime;
    private int x, y;

    public Image icon;

    public MinesGame(int x, int y, int nmines) {
        FIELD_X = x;
        FIELD_Y = y;
        NUM_MINES = nmines;
        WIDTH = FIELD_X * (MINE_SIZE + BUFFER) + EDGE * 2;
        HEIGHT = FIELD_Y * (MINE_SIZE + BUFFER) + EDGE * 2;
        if (WIDTH < MIN_WIDTH) WIDTH = MIN_WIDTH;
        if (HEIGHT < MIN_HEIGHT) HEIGHT = MIN_HEIGHT;

        Dimension size = new Dimension(WIDTH, HEIGHT);
        setPreferredSize(size);
        setMaximumSize(size);
        setMinimumSize(size);

        URL url = ClassLoader.getSystemResource("MinesLogo.png");
        Toolkit kit = Toolkit.getDefaultToolkit();
        icon = kit.createImage(url);

        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        screen = new Screen(WIDTH, HEIGHT);
        mines = new Minefield(FIELD_X, FIELD_Y, NUM_MINES, MINE_SIZE, MINE_SIZE, BUFFER, EDGE, screen);

        score = NUM_MINES;

        clock = new Clock();
        mouse = new Mouse();
        addMouseListener(mouse);

    }

    public synchronized void start() {
        if (running) return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() {
        if (!running) return;
        running = false;
        // try {
        // thread.join();
        // } catch (InterruptedException e) {
        // e.printStackTrace();
        // }
    }

    public void run() {
        requestFocus();
        long lastTime = System.nanoTime();
        long timer = System.currentTimeMillis();
        final double ns = 1000000000.0 / 60.0;
        double delta = 0;
        int frames = 0;
        int ticks = 0;
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;

            while (delta >= 1) {
                tick();
                ticks++;
                delta--;
            }

            render();
            frames++;

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                tps = ticks;
                fps = frames;
                ticks = 0;
                frames = 0;
            }
        }
    }

    private void tick() {
        boolean mine = false;
        if (mouse.middleClick) {
            x = mouse.x;
            y = mouse.y;
            mine = mines.clearSurround(x, y);
            mouse.reset();
        } else if (mouse.leftClick) {
            firstClick = true;
            if (firstClick && !clockStarted) clock.start();
            x = mouse.x;
            y = mouse.y;
            if (x > 10 && x < 90 && y < (getHeight() - 10) && y > (getHeight() - 30)) {
                options();
            } else {
                mine = mines.checkMine(x, y);
            }
            mouse.reset();
        } else if (mouse.rightClick) {
            x = mouse.x;
            y = mouse.y;
            int marked = mines.markMine(x, y);
            if (marked == 1) score += 1;
            if (marked == 2) score -= 1;
            mouse.reset();
        }
        if ((mine || mines.checkWin()) && !gameOver) {
            gameOver = true;
            clock.stop();
            loss = mine;
            elapsedTime = clock.getElapsedSeconds();
            gameOver();
        }

        clock.update();
        mines.draw();
    }

    private void render() {
        bs = this.getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            return;
        }

        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = screen.pixels[i];
        }

        try {
            Graphics g = bs.getDrawGraphics();
            g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
            drawLabels(g);

            if (debug) {
                g.setFont(new Font("", Font.PLAIN, 12));
                g.setColor(Color.BLACK);
                g.fillRect(0, 0, 120, 64);
                g.setColor(Color.GREEN);
                g.drawString("FPS: " + fps, 10, 10);
                g.drawString("TPS: " + tps, 10, 23);
                g.drawString("Selected: " + x + " | " + y, 10, 36);
                g.drawString("Remaining: " + score, 10, 49);
                g.drawString("Clock: " + clock.getElapsedSeconds(), 10, 62);
            }

            g.dispose();
            bs.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void drawLabels(Graphics g) {
        g.setColor(new Color(0x350CA6));
        g.setFont(new Font("Courier New", Font.BOLD, 20));
        g.drawString("Remaining: " + score, 10, 25);
        g.drawString("Time: " + clock.getElapsedSeconds(), getWidth() - 125, 25);
        g.drawString("Options", 10, getHeight() - 10);
    }

    private void gameOver() {
        mines.showAll();
        Thread t = new Thread() {
            public void run() {
                played++;
                String message;
                int bestTime = options.getBestTime(NUM_MINES, FIELD_X, FIELD_Y);
                if (loss) message = "You lose...";
                else {
                    message = "You Win!";
                    won++;
                    if (elapsedTime < bestTime) bestTime = elapsedTime;
                }
                int percent = ((won * 100) / played);
                String line = message + "\nPlayed: " + played + "  Won: " + won + "  Percent: " + percent + "%" + "\nBest Time: " + bestTime + "\nPlay again?";
                int response = JOptionPane.showConfirmDialog(frame, line, message, JOptionPane.YES_NO_OPTION);
                options.write(NUM_MINES, FIELD_X, FIELD_Y, played, won, bestTime);
                if (response == 0) {
                    mines = new Minefield(FIELD_X, FIELD_Y, NUM_MINES, MINE_SIZE, MINE_SIZE, BUFFER, EDGE, screen);
                    gameOver = false;
                    clock.reset();
                    firstClick = false;
                    score = NUM_MINES;
                }
                try {
                    join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }

    private static void options() {
        int nmines = Integer.parseInt(JOptionPane.showInputDialog(null, "Number of Mines:"));
        int sx = Integer.parseInt(JOptionPane.showInputDialog(null, "Minefield width:"));
        int sy = Integer.parseInt(JOptionPane.showInputDialog(null, "Minefield height:"));
        NUM_MINES = nmines;
        FIELD_X = sx;
        FIELD_Y = sy;
        options.write(nmines, sx, sy, played, won, options.getBestTime(nmines, sx, sy));
        frame.setVisible(false);
        frame.remove(t);
        t.stop();
        t = null;
        t = new MinesGame(options.getSizeX(), options.getSizeY(), options.getMines());
        frame.addWindowListener(t.clock);
        frame.setIconImage(t.icon);
        frame.add(t);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        t.start();
    }

    public static void main(String args[]) {
        if (args.length > 0) {
            if (args[0].equals("-debug")) debug = true;
        }
        options = new OptionsFile("mines.opt");
        boolean exist = options.read();
        if (!exist) {
            options.write(99, 30, 16, 0, 0, 9999);
            options.read();
        }
        played = options.getPlayed();
        won = options.getWon();
        t = new MinesGame(options.getSizeX(), options.getSizeY(), options.getMines());
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        if (debug) frame.setTitle(GAME_TITLE + " | " + GAME_STAGE + " " + GAME_MAJOR_VERSION + "." + GAME_MINOR_VERSION + "." + GAME_REVISION);
        else
            frame.setTitle(GAME_TITLE);
        frame.addWindowListener(t.clock);
        frame.setIconImage(t.icon);
        frame.add(t);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        t.start();
    }

}
