package com.trevor.king.Mines.graphics;

public class Screen {

    private int width;
    public int[] pixels;

    public Screen(int width, int height) {
        this.width = width;
        pixels = new int[width * height];
        clear();
    }

    public void clear() {
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = 0xaaaaaa;
        }
    }

    public void render(int x, int y, int iWidth, int iHeight, int[] iPix) {
        for (int xx = 0; xx < iWidth; xx++) {
            for (int yy = 0; yy < iHeight; yy++) {
                pixels[(xx + x) + (yy + y) * width] = iPix[xx + yy * iWidth];
            }
        }
    }

}
