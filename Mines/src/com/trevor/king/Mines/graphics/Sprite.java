package com.trevor.king.Mines.graphics;

public class Sprite {

    public final int SIZE;
    private int x, y;
    public int[] pixels;
    private SpriteSheet sheet;

    public static Sprite normal = new Sprite(32, 0, 0, SpriteSheet.mines);
    public static Sprite one = new Sprite(32, 1, 0, SpriteSheet.mines);
    public static Sprite two = new Sprite(32, 2, 0, SpriteSheet.mines);
    public static Sprite three = new Sprite(32, 3, 0, SpriteSheet.mines);
    public static Sprite four = new Sprite(32, 4, 0, SpriteSheet.mines);
    public static Sprite five = new Sprite(32, 5, 0, SpriteSheet.mines);
    public static Sprite six = new Sprite(32, 6, 0, SpriteSheet.mines);
    public static Sprite seven = new Sprite(32, 7, 0, SpriteSheet.mines);
    public static Sprite eight = new Sprite(32, 8, 0, SpriteSheet.mines);
    public static Sprite zero = new Sprite(32, 9, 0, SpriteSheet.mines);
    public static Sprite mine = new Sprite(32, 10, 0, SpriteSheet.mines);
    public static Sprite flag = new Sprite(32, 11, 0, SpriteSheet.mines);

    public Sprite(int size, int x, int y, SpriteSheet sheet) {
        this.SIZE = size;
        pixels = new int[SIZE * SIZE];
        this.x = x * size;
        this.y = y * size;
        this.sheet = sheet;
        load();
    }

    private void load() {
        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {
                pixels[x + y * SIZE] = sheet.pixels[(x + this.x) + (y + this.y) * sheet.SIZE_X];
            }
        }
    }
}
